﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestIdentity.Models.BusinessViewModels;

namespace TestIdentity.Services
{
    public class SurvyeService : ISurvyeService
    {
        public List<SurveyProduct> GetSurvyeDetails()
        {
            var survye = new List<SurveyProduct>()
            {
                new SurveyProduct() { Id = 1, Name = "Hoodies", RateCount = 8 },
                new SurveyProduct() { Id = 2, Name = "Banners", RateCount = 1 },
                new SurveyProduct() { Id = 3, Name = "Posters", RateCount = 4 },
                new SurveyProduct() { Id = 4, Name = "T-Shirts", RateCount = 2 },
            };

            return survye;
        }
    }
}
