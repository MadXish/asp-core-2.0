﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestIdentity.Models.BusinessViewModels;

namespace TestIdentity.Services
{
    public interface ISurvyeService
    {
        List<SurveyProduct> GetSurvyeDetails();
    }
}
