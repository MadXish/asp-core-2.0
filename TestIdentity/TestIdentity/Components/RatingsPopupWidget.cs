using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestIdentity.Services;

namespace TestIdentity.Components
{
    public class RatingsPopupWidget : ViewComponent
    {
        private ISurvyeService _isurvyeService;

        public RatingsPopupWidget(ISurvyeService isurvyeService)
        {
            _isurvyeService = isurvyeService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View(_isurvyeService.GetSurvyeDetails());
        }
    }
}