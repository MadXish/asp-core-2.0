﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestIdentity.Models.BusinessViewModels
{
    public class SurveyProduct
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal RateCount { get; set; }
    }
}
