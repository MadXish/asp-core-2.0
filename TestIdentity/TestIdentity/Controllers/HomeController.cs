﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestIdentity.Models;

namespace TestIdentity.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        [Route("en/Home/Index")]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        [Route("ar/Home/Index")]
        public IActionResult IndexAr()
        {
            return View();
        }


        [Route("en/Home/About")]
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        [Route("ar/Home/About")]
        public IActionResult AboutAr()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }


        [Route("en/Home/Contact")]
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        [Route("ar/Home/Contact")]
        public IActionResult ContactAr()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }


        public IActionResult ChangeLanguage(string lang, string currentPath)
        {
            if (!string.IsNullOrEmpty(currentPath))
            {
                var language = currentPath.Split('/');
                if (lang == "ar")
                {
                    language[1] = "ar";
                }
                else
                {
                    language[1] = "en";
                }
                currentPath = string.Join("/", language);
                return Redirect(currentPath);
            }
            return Redirect(currentPath);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public decimal rate(decimal rate)
        {
            return rate; 
        }
    }
}
