﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace TestIdentity.Controllers
{
    public class AdminController : Controller
    {
        [Authorize(Roles ="Admin")]
        [Route("en/Admin/Index")]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        [Route("ar/Admin/Index")]
        public IActionResult IndexAr()
        {
            return View();
        }
    }
}